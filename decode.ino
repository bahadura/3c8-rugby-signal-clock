#define DATA_LEN 60
int data[DATA_LEN] = {0};

#define SIGNAL_PIN 13 // onboard LED will flash too

int current_state = 0, zero_count = 0, one_count = 0;
bool first = 0;
// current state 2
void setup() {
  Serial.begin(115200);
  //pinMode(SIGNAL_PIN, OUTPUT);
  pinMode(SIGNAL_PIN, INPUT);

}

void loop() {

  // check for minute marker
  // low for 500ms

  // looking for sequence 0 0 0 0 0 1 1 1 1 1
  int value;

  value = digitalRead(SIGNAL_PIN);
  Serial.println(value);
  delay(100);

  //Serial.println("here");
  //Serial.println(first);
  
  if(current_state == 0){ // looking for zeros
  first = 0;
    if(value == LOW){ // signal value is zero
      zero_count++; // increment zero count
      if(zero_count == 5){ // 5 consecutive zeros encountered
        Serial.println("5 zeros");
        current_state = 1; // switch to looking for ones
        zero_count = 0; // reset zero count
      }
    } else{ //not consecutive zeros
      zero_count = 0; // reset zero count
    }
  }

  if(current_state == 1){ // looking for ones
  //Serial.println("looking for 1s");
  //Serial.println(first);
    if(first){
      //Serial.println(value);
      //Serial.println("inside first loop");
      if(value == HIGH){ //signal value is one
      one_count++; //increment one count
      //Serial.println("counting 1s");
      if(one_count == 5){ // 5 consecuive ones encountered
        //Serial.println("5 ones");
        current_state = 2; // store A bits
        one_count = 0; // reset one count
        Serial.println("minute marker found");
        first = 0;
        //send pulse
        //start reading data
      }
    } else{ //not consecutive ones after 5 zeros
      current_state = 0; // go back to looking for zeros
      one_count = 0; // reset one count
      //first = 0;
      //Serial.println("first reset");
    }

    }

    first = 1;
    
  }
  
  //after first second begin storing bit A
  if(current_state == 2){
    for(int i = 1; i < 60; i++){
      delay(100);
      value = digitalRead(SIGNAL_PIN);
      if(!value){
        data[i] = 1;
      }
      else{
        data[i] = 0;
      }
      delay(900);
    }

  //Serial.println("time out");
  display_time();
  current_state = 0; //check for next minute marker
  //delay(800);
    
  }


}


void display_time(){
  int hour = 0, minute = 0; 
  //calculate hour
  //Serial.println(hour);
  int print_data39 = data[39];
  hour = data[39]*20;
  hour += data[40]*10;
  hour += data[41]*8;
  hour += data[42]*4;
  hour += data[43]*2;
  hour += data[44]*1;

  //calculate minute
  minute = data[45]*40;
  minute += data[46]*20;
  minute += data[47]*10;
  minute += data[48]*8;
  minute += data[49]*4;
  minute += data[50]*2;
  minute += data[51]*1;

  //Send to screen and hold for 60secs
  Serial.println(hour);
  Serial.println(minute);

}
